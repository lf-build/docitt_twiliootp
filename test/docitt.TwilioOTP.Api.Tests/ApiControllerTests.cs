﻿using System;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using Docitt.TwilioOTP;
using Docitt.TwilioOTP.Api.Controllers;
using System.Threading.Tasks;
using System.Collections.Generic;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Email.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<IAuthyProxy> authyProxy { get; }
        private IOTPService otpService { get; }
        private ApiController apiController { get; set; }

        private Mock<IEventHubClient> eventHubClient;

        public ApiControllerTests()
        {
            authyProxy = new Mock<IAuthyProxy>();

            eventHubClient = new Mock<IEventHubClient>();

            otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
        }

        [Fact]
        public async void SendOTP_WithNullOrEmptyPhoneNumber_ThrowsArgumentException()
        {
            var countryCode = "91";
            var codeLength = "4";
            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (ErrorResult)await apiController.SendOTP(null, countryCode, codeLength);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.SendOTP(" ", countryCode, codeLength);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.SendOTP(string.Empty, countryCode, codeLength);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);           
        }

        [Fact]
        public async void SendOTP_WithNullOrEmptyCountryCode_ThrowsArgumentException()
        {
            var phoneNumber = "9123445123";
            var codeLength = "4";
            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (ErrorResult)await apiController.SendOTP(phoneNumber, null, codeLength);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.SendOTP(phoneNumber, " ", codeLength);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.SendOTP(phoneNumber, string.Empty, codeLength);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        
        [Fact]
        public async void Send_OTP_WithValidPhoneNumberCountry_ReturnsSuccessWithOTPResult()
        {
            var phoneNumber = "9687485287";
            var countryCode = "91";
            var codeLength = "4";
            authyProxy.Setup(s => s.SendOTP(phoneNumber, countryCode, codeLength))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = true,
                        Error_Code = string.Empty,
                        Message = "SMS Sent to " + phoneNumber,
                        Status = OTPStatus.Success
                    }));

            var otpService = new OTPService(authyProxy.Object,eventHubClient.Object);            
            apiController = new ApiController(otpService);
            var result = (HttpOkObjectResult)await apiController.SendOTP(phoneNumber, countryCode, codeLength);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(((OTPResult)result.Value).Success, true);
        }

        [Fact]
        public async void Send_OTP_WithInValidPhoneNumber_ReturnsSuccessStatusCodeWithOTPResult_ErrorCode()
        {
            var phoneNumber = "9687485287";
            var countryCode = "91";
            var codeLength = "4";
            var Errormessage = new Dictionary<string, string>()
            {
                { "message","Phone number is invalid"}
            };

            authyProxy.Setup(s => s.SendOTP(phoneNumber, countryCode, codeLength))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = false,
                        Error_Code = "60033",
                        Message = "Phone number is invalid",
                        Status = OTPStatus.Success,
                        Errors = Errormessage
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (HttpOkObjectResult)await apiController.SendOTP(phoneNumber, countryCode, codeLength);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(((OTPResult)result.Value).Success, false);
            Assert.Equal(((OTPResult)result.Value).Error_Code, "60033");
        }

        [Fact]
        public async void VerifyOTP_WithNullOrEmptyPhoneNumber_ThrowsArgumentException()
        {
            var countryCode = "91";
            var verificationCode = "12345";
            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (ErrorResult)await apiController.VefifyOTP(null, countryCode, verificationCode);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(" ", countryCode, verificationCode);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(string.Empty, countryCode, verificationCode);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void VerifyOTP_WithNullOrEmptyCountryCode_ThrowsArgumentException()
        {
            var phoneNumber = "9123445123";           
            var verificationCode = "12345";
            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, null, verificationCode);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, " ", verificationCode);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, string.Empty, verificationCode);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void VerifyOTP_WithNullOrEmptyVerificationCode_ThrowsArgumentException()
        {
            var phoneNumber = "9123445123";
            var countryCode = "91";
            
            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            apiController = new ApiController(otpService);
            var result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, countryCode, null);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, countryCode, " ");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult)await apiController.VefifyOTP(phoneNumber, countryCode, string.Empty);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void VerifyOTP_WithValidVerificationCode_WhenSuccess_ReturnsOTPResult_WithSuccess()
        {
            var phoneNumber = "9687485287";
            var countryCode = "91";
            var verificationCode = "1234";

            var Errormessage = new Dictionary<string, string>();


            authyProxy.Setup(s => s.VerifyOTP(phoneNumber, countryCode, verificationCode))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = true,
                        Error_Code = null,
                        Message = "Verification code is correct.",
                        Status = OTPStatus.Success,
                        Errors = null
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            
            apiController = new ApiController(otpService);
            var result = (HttpOkObjectResult)await apiController.VefifyOTP(phoneNumber, countryCode,verificationCode);


            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(((OTPResult)result.Value).Success, true);            
        }

        [Fact]
        public async void VerifyOTP_WithInValidVerificationCode_WhenSuccess_ReturnsOTPResult_WithSuccess()
        {
            var phoneNumber = "9687485287";
            var countryCode = "91";
            var verificationCode = "12345";

            var Errormessage = new Dictionary<string, string>()
            {
                {  "message", "Verification code is incorrect"}
            };


            authyProxy.Setup(s => s.VerifyOTP(phoneNumber, countryCode, verificationCode))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = false,
                        Error_Code = "60022",
                        Message = "Verification code is incorrect",
                        Status = OTPStatus.Success,
                        Errors = Errormessage
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var result = (HttpOkObjectResult)await apiController.VefifyOTP(phoneNumber, countryCode, verificationCode);

            Assert.NotNull(result);        
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(((OTPResult)result.Value).Success, false);
            Assert.Equal(((OTPResult)result.Value).Error_Code, "60022");
        }
    }
}
