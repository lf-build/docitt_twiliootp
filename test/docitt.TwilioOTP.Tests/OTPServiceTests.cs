﻿using System;
using Moq;
using Xunit;
using System.Threading.Tasks;
using System.Collections.Generic;
using LendFoundry.EventHub.Client;

namespace Docitt.TwilioOTP.Tests
{
    public class OTPServiceTests
    {      
        private Mock<IAuthyProxy> authyProxy { get; }
        private IOTPService otpService { get; }

        private Mock<IEventHubClient> eventHubClient;

        public OTPServiceTests()
        {
            authyProxy = new Mock<IAuthyProxy>();
            eventHubClient = new Mock<IEventHubClient>();

             otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
        }

        [Fact]
        public async void SendOTP_WithNullOrEmptyCountryCode_ThrowsArgumentException()
        {
            string countryCode = "91";
            var codeLength = "4";
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(null, countryCode,codeLength));
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(" ", countryCode, codeLength));
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(string.Empty, countryCode, codeLength));
        }

        [Fact]
        public async void SendOTP_WithNullOrEmptyPhoneNumber_ThrowsArgumentException()
        {
            string phoneNumber = "9687485287";
            var codeLength = "4";
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(phoneNumber, null, codeLength));
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(phoneNumber, " ", codeLength));
            await Assert.ThrowsAsync<ArgumentException>(() => otpService.SendOTP(phoneNumber, string.Empty, codeLength));
        }

        [Fact]
        public async void SendOTP_WhenSuccess_ReturnsOTPResult()
        {
            var phoneNumber = "9687485287";
            var countryCode = "91";
            var codeLength = "4";
            authyProxy.Setup(s => s.SendOTP(phoneNumber, countryCode, codeLength))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = true,
                        Error_Code = string.Empty,
                        Message ="SMS Sent to " + phoneNumber,
                        Status = OTPStatus.Success
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var otpResult = await otpService.SendOTP(phoneNumber,countryCode, codeLength);

            Assert.NotNull(otpResult);
            Assert.Equal(otpResult.Success, true);
            Assert.Equal(otpResult.Error_Code, string.Empty);
            Assert.Equal(otpResult.Status, OTPStatus.Success);
        }

        [Fact]
        public async void SendOTP_WithWrongPhoneNumber_WhenSuccess_ReturnsOTPResult_WithErrorCode_ErrorMessage()
        {
            var phoneNumber = "96874852871";
            var countryCode = "91";
            var codeLength = "4";
            var Errormessage = new Dictionary<string, string>()
            {
                { "message","Phone number is invalid"}
            };

            authyProxy.Setup(s => s.SendOTP(phoneNumber, countryCode, codeLength))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = false,
                        Error_Code = "60033",
                        Message = "Phone number is invalid",
                        Status = OTPStatus.Success,
                        Errors = Errormessage
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var otpResult = await otpService.SendOTP(phoneNumber, countryCode, codeLength);

            Assert.NotNull(otpResult);
            Assert.Equal(otpResult.Success, false);
            Assert.Equal(otpResult.Error_Code, "60033");
            Assert.Equal(otpResult.Message, "Phone number is invalid");
        }

        [Fact]
        public async void SendOTP_WithWrongCountryCode_WhenSuccess_ReturnsOTPResult_WithErrorCode_ErrorMessage()
        {
            var phoneNumber = "9687485287";
            var countryCode = "911";
            var codeLength = "4";

            var Errormessage = new Dictionary<string, string>()
            {
                { "message","Invalid country code"}
            };

            authyProxy.Setup(s => s.SendOTP(phoneNumber, countryCode, codeLength))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = false,
                        Error_Code = "60078",
                        Message = "Invalid country code",
                        Status = OTPStatus.Success,
                        Errors = Errormessage
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var otpResult = await otpService.SendOTP(phoneNumber, countryCode, codeLength);

            Assert.NotNull(otpResult);
            Assert.Equal(otpResult.Success, false);
            Assert.Equal(otpResult.Error_Code, "60078");
            Assert.Equal(otpResult.Message, "Invalid country code");
        }

        [Fact]
        public async void VerifyOTP_WithValidVerificationCode_WhenSuccess_ReturnsOTPResult_WithSuccess()
        {
            var phoneNumber = "9687485287";
            var countryCode = "911";
            var verificationCode = "1234";

            var Errormessage = new Dictionary<string, string>();
            

            authyProxy.Setup(s => s.VerifyOTP(phoneNumber, countryCode,verificationCode))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = true,
                        Error_Code = null,
                        Message = "Verification code is correct.",
                        Status = OTPStatus.Success,
                        Errors = null
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var otpResult = await otpService.VerifyOTP(phoneNumber, countryCode,verificationCode);

            Assert.NotNull(otpResult);
            Assert.Equal(otpResult.Success, true);
            Assert.Equal(otpResult.Error_Code, null);
            Assert.Equal(otpResult.Message, "Verification code is correct.");
            Assert.Equal(otpResult.Errors, null);
        }

        [Fact]
        public async void VerifyOTP_WithInValidVerificationCode_WhenSuccess_ReturnsOTPResult_WithSuccess()
        {
            var phoneNumber = "9687485287";
            var countryCode = "911";
            var verificationCode = "12345";

            var Errormessage = new Dictionary<string, string>()
            {
                {  "message", "Verification code is incorrect"}
            };


            authyProxy.Setup(s => s.VerifyOTP(phoneNumber, countryCode, verificationCode))
                .Returns(() => Task.FromResult<OTPResult>(
                    new OTPResult
                    {
                        Success = false,
                        Error_Code = "60022",
                        Message = "Verification code is incorrect",
                        Status = OTPStatus.Success,
                        Errors = Errormessage
                    }));

            var otpService = new OTPService(authyProxy.Object, eventHubClient.Object);
            var otpResult = await otpService.VerifyOTP(phoneNumber, countryCode,verificationCode);

            Assert.NotNull(otpResult);
            Assert.Equal(otpResult.Success, false);
            Assert.Equal(otpResult.Error_Code, "60022");
            Assert.Equal(otpResult.Message, "Verification code is incorrect");
            Assert.Equal(otpResult.Errors.Count, 1);
        }
    }
}
