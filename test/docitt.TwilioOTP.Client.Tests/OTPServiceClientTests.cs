﻿
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.TwilioOTP.Client.Tests
{
    public class OTPServiceClientTests
    {
        private IOTPService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public OTPServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new OTPService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_Send_OTP()
        {

            ServiceClient.Setup(s => s.ExecuteAsync<OTPResult>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(new OTPResult
                        {
                            Success = true,
                            Error_Code = string.Empty,
                            Message = "SMS Sent to 9998931362",
                            Status = OTPStatus.Success
                        }));

          
            var result = await Client.SendOTP("9998931362", "91","4");

            Assert.Equal("{phoneNumber}/{countryCode}/{code_length}/sendotp", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Verify_OTP()
        {

            ServiceClient.Setup(s => s.ExecuteAsync<OTPResult>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(new OTPResult
                        {
                            Success = true,
                            Error_Code = null,
                            Message = "Verification code is correct.",
                            Status = OTPStatus.Success,
                            Errors = null
                        }));


            var result = await Client.VerifyOTP("9998931362", "91","1234");
                       
            Assert.Equal("{phoneNumber}/{countryCode}/{verificationCode}/verify", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }
    }
}
