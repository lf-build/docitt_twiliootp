﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Docitt.TwilioOTP
{
    public interface ITextMessageRequest
    {
        string CountryCode { get; set; }
        string PhoneNumber { get; set; }
        string Body { get; set; }
    }
}
