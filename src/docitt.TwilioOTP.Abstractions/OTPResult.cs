﻿using System.Collections.Generic;

namespace Docitt.TwilioOTP
{
    /// <summary>
    ///  OTP Result Class
    /// </summary>
    public class OTPResult : IOTPResult
    {
        /// <summary>
        /// The status of a request
        /// </summary>
        public OTPStatus Status { get; set; }

        /// <summary>
        /// The request was success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The message from the API
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The message from the API
        /// </summary>
        public string Error_Code { get; set; }
        /// <summary>
        /// The list of erros
        /// <summary>
        public Dictionary<string, string> Errors { get; set; }
    }
}
