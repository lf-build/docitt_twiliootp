﻿using System.Collections.Generic;

namespace Docitt.TwilioOTP
{
    public interface IOTPResult
    {
        Dictionary<string, string> Errors { get; set; }
        string Error_Code { get; set; }
        string Message { get; set; }
        OTPStatus Status { get; set; }
        bool Success { get; set; }
    }
}