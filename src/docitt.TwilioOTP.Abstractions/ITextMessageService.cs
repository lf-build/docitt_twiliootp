﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Docitt.TwilioOTP
{
    public interface ITextMessageService
    {
        Task<ITextMessageResponse> SendTextMessage(TextMessageRequest request);
    }
}
