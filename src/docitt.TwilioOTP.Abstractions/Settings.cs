﻿using System;

namespace Docitt.TwilioOTP
{
    public static class Settings
    {       
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "twilio-otp";       
    }
}
