﻿using System.Threading.Tasks;

namespace Docitt.TwilioOTP
{
    public interface IOTPService
    {
        Task<IOTPResult> SendOTP(string phoneNumber, string countryCode,string codeLength);
        Task<IOTPResult> VerifyOTP(string phoneNumber, string countryCode, string verificationCode);
    }
}