﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Docitt.TwilioOTP
{
    public interface ITextMessageResponse
    {
        string Sid { get; set; }
    }
}
