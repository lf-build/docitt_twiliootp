﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Docitt.TwilioOTP
{
    public class TextMessageRequest : ITextMessageRequest
    {
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Body { get; set; }
    }
}
