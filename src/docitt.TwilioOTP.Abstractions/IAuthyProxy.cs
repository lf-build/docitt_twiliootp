﻿using RestSharp;
using System.Threading.Tasks;

namespace Docitt.TwilioOTP
{
    public interface IAuthyProxy
    {
        Task<T> ExecuteAsync<T>(RestRequest request, string url) where T : new();
        Task<OTPResult> SendOTP(string phoneNumber, string countryCode,string codeLength);
        Task<OTPResult> VerifyOTP(string phoneNumber, string countryCode, string verificationCode);
    }
}