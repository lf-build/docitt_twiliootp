﻿using System.Collections.Generic;

namespace Docitt.TwilioOTP.Configuration
{
    public class TwilioConfiguration : ITwilioConfiguration
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Api_Key { get; set; }

        public string BaseUrl { get; set; }

        public string SendOTPUrl { get; set; }

        public string VerifyOTP { get; set; }

        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
        public string FromNumberForText { get; set; }
        public bool EnableTextMessage {get;set;}
    }
}
