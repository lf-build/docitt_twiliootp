﻿using LendFoundry.Foundation.Client;
namespace Docitt.TwilioOTP.Configuration
{
    public interface ITwilioConfiguration : IDependencyConfiguration
    {
        string AccountSid { get; set; }
        string AuthToken { get; set; }
        string Api_Key { get; set; }

        string BaseUrl { get; set; }

        string SendOTPUrl { get; set; }

        string VerifyOTP { get; set; }
        string FromNumberForText { get; set; }

         bool EnableTextMessage {get;set;}
    }
}
