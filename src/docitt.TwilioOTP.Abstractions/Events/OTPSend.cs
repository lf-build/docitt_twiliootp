﻿namespace Docitt.TwilioOTP.Events
{
    public class OTPSend
    {
        public string MobileNumber { get; set; }

        public string CountryCode { get; set; }

        public object ResponseData { get; set; }
    }
}
