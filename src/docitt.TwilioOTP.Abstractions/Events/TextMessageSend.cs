namespace Docitt.TwilioOTP.Events
{
    public class TextMessageSend
    {
        public string FromPhoneNumber { get; set; }

        public string ToMobileNumber { get; set; }

        public string CountryCode { get; set; }

        public string Body {get; set;}

        public object ResponseData { get; set; }
    }
}