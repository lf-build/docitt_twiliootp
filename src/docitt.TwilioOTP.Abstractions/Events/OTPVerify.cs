﻿namespace Docitt.TwilioOTP.Events
{
    public class OTPVerify : OTPSend
    {
        public string VerificationCode { get; set; }
    }
}
