﻿using System;
using System.Runtime.Serialization;

namespace Docitt.TwilioOTP
{
    [Serializable]
    public class OtpException : Exception
    {
        public OtpException()
        {
        }

        public OtpException(string message) : base(message)
        {
        }

        public OtpException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OtpException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

    }
}
