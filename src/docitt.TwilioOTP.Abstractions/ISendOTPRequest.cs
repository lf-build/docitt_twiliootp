﻿namespace Docitt.TwilioOTP
{
    public interface ISendOTPRequest
    {
        string country_code { get; set; }
        string locale { get; set; }
        string phone_number { get; set; }
        string via { get; set; }
    }
}