﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Docitt.TwilioOTP
{
    public class TextMessageResponse : ITextMessageResponse
    {
        public string Sid { get; set; }
    }
}
