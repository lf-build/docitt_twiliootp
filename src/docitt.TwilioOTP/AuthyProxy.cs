﻿using System;
using System.Threading.Tasks;
using RestSharp;
using Docitt.TwilioOTP.Configuration;

namespace Docitt.TwilioOTP
{
    /// <summary>
    ///  The OTP Send / verify Proxy Class
    /// </summary>
    public class AuthyProxy : IAuthyProxy
    {

        private ITwilioConfiguration Configuration { get; }

        public AuthyProxy(ITwilioConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            if (string.IsNullOrWhiteSpace(configuration.Api_Key))
            {
                throw new ArgumentException("API Key cannot be empty", nameof(configuration.Api_Key));
            }

            if (string.IsNullOrWhiteSpace(configuration.BaseUrl))
            {
                throw new ArgumentException("API Key cannot be empty", nameof(configuration.BaseUrl));
            }

            if (string.IsNullOrWhiteSpace(configuration.SendOTPUrl))
            {
                throw new ArgumentException("API Key cannot be empty", nameof(configuration.SendOTPUrl));
            }

            if (string.IsNullOrWhiteSpace(configuration.VerifyOTP))
            {
                throw new ArgumentException("API Key cannot be empty", nameof(configuration.VerifyOTP));
            }

            Configuration = configuration;
        }
        /// <summary>
        /// Send OTP 
        /// </summary>
        /// <param name="pboneNumber">The phone number</param>
        /// <param name="countryCode"></param>
        /// <param name="codeLength">OTP Code Length</param>
        /// <returns>OTP Result object</returns>
        public Task<OTPResult> SendOTP(string phoneNumber, string countryCode,string codeLength)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(phoneNumber));

            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(countryCode));

            if (string.IsNullOrWhiteSpace(codeLength))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(codeLength));

            var restRequest = new RestRequest(Method.POST);

            var otpRequest = new SendOTPRequest
            {
                via = "sms",
                phone_number = phoneNumber,
                country_code = countryCode
            };

            var json = restRequest.JsonSerializer.Serialize(otpRequest);

            restRequest.AddParameter("text/json", json, ParameterType.RequestBody);

            string url = Configuration.BaseUrl + Configuration.SendOTPUrl + "?api_key=" + Configuration.Api_Key + "&code_length=" + codeLength;
            var objResult = ExecuteAsync<OTPResult>(restRequest, url);

            return objResult;
        }

        /// <summary>
        /// Verify OTP
        /// </summary>
        /// <param name="phoneNumber">phone number for which OTP to be verify</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="verificationCode">Verification code</param>
        /// <returns>Return OTPResult object</returns>
        public Task<OTPResult> VerifyOTP(string phoneNumber, string countryCode, string verificationCode)
        {

            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(phoneNumber));

            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(countryCode));

            if (string.IsNullOrWhiteSpace(verificationCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(verificationCode));
            
            string url = Configuration.BaseUrl + Configuration.VerifyOTP + "?api_key=" + Configuration.Api_Key;
            var restRequest = new RestRequest(Method.GET);

            restRequest.AddQueryParameter("phone_number", phoneNumber);
            restRequest.AddQueryParameter("country_code", countryCode);
            restRequest.AddQueryParameter("verification_code", verificationCode);
            var objResult = ExecuteAsync<OTPResult>(restRequest, url);

            return objResult;

        }

        /// <summary>
        ///  Execute async rest request
        /// </summary>
        /// <typeparam name="T">T object</typeparam>
        /// <param name="request">RestRequest object</param>
        /// <param name="url">URL of the api</param>
        /// <returns>Return T object</returns>
        public Task<T> ExecuteAsync<T>(RestRequest request, string url) where T : new()
        {
            var client = new RestClient();
            var taskResult = new TaskCompletionSource<T>();
            client.BaseUrl = new Uri(url);
            client.ExecuteAsync<T>(request, response =>
            {
                taskResult.SetResult(response.Data);
            }
            );
            return taskResult.Task;
        }
    }
}
