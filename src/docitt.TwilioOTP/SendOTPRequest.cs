﻿namespace Docitt.TwilioOTP
{
    /// <summary>
    /// Send OTP Request Class
    /// </summary>
    public class SendOTPRequest : ISendOTPRequest
    {
        /// <summary>
        ///  Send OTP Via , means sms or call.
        /// </summary>
        public string via { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        public string phone_number { get; set; }

        /// <summary>
        /// Country Code
        /// </summary>
        public string country_code { get; set; }

        /// <summary>
        /// locale (Ex , en for english , es for spanish)
        /// </summary>
        public string locale { get; set; }

    }
}
