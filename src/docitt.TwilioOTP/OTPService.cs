﻿using LendFoundry.EventHub;

using System;
using System.Threading.Tasks;

namespace Docitt.TwilioOTP
{
    /// <summary>
    /// OTP Service Class
    /// </summary>
    public class OTPService : IOTPService
    {
        private IAuthyProxy  authyProxy {get;}

        private IEventHubClient EventHubClient { get; }

        public OTPService(IAuthyProxy authProxy, IEventHubClient eventHubClient)
        {
            authyProxy = authProxy;
            EventHubClient = eventHubClient;
        }
        /// <summary>
        ///  Send OTP
        /// </summary>
        /// <param name="phoneNumber">Phone number to send OTP</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="codeLength">OTP Code Length</param>
        /// <returns>Return OTPResult object</returns>
        public async Task<IOTPResult> SendOTP(string phoneNumber, string countryCode,string codeLength)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(phoneNumber));
            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentException("Argument is null or whitespace", nameof(countryCode));

            if (string.IsNullOrWhiteSpace(codeLength))
                throw new ArgumentException("Argument is null or whitespace", nameof(codeLength));


            var result = await authyProxy.SendOTP(phoneNumber, countryCode,codeLength);

            await EventHubClient.Publish(new Events.OTPSend
            {
                MobileNumber = phoneNumber,
                CountryCode = countryCode,
                ResponseData = result
            });

            return result;
        }

        /// <summary>
        ///  Verify OTP
        /// </summary>
        /// <param name="phoneNumber">Phone number</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="verificationCode">Verification code</param>
        /// <returns>Return OTPResult object</returns>

        public async Task<IOTPResult> VerifyOTP(string phoneNumber, string countryCode,string verificationCode)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(phoneNumber));

            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentException("Argument is null or whitespace", nameof(countryCode));

            if (string.IsNullOrWhiteSpace(verificationCode))
                throw new ArgumentException("Argument is null or whitespace", nameof(verificationCode));
                      
            var result = await authyProxy.VerifyOTP(phoneNumber, countryCode, verificationCode);

            await EventHubClient.Publish(new Events.OTPVerify
            {
                MobileNumber = phoneNumber,
                CountryCode = countryCode,
                ResponseData = result,
                VerificationCode=verificationCode
            });
            return result;
        }
    }
}
