﻿using Docitt.TwilioOTP;
using Docitt.TwilioOTP.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Docitt.TwilioOTP
{
    public class TextMessageService : ITextMessageService
    {
        private IEventHubClient EventHubClient { get; }
        private ITwilioConfiguration Configuration { get; }
        private ILogger Logger { get; }
        public TextMessageService(ITwilioConfiguration configuration, IEventHubClient eventHubClient, ILogger logger)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            if (string.IsNullOrWhiteSpace(configuration.AccountSid))
            {
                throw new ArgumentException("AccountSid cannot be empty", nameof(configuration.AccountSid));
            }
            if (string.IsNullOrWhiteSpace(configuration.AuthToken))
            {
                throw new ArgumentException("AuthToken cannot be empty", nameof(configuration.AuthToken));
            }

            Configuration = configuration;
            EventHubClient = eventHubClient;
            Logger = logger;
        }

        public async Task<ITextMessageResponse> SendTextMessage(TextMessageRequest request)
        {
            ITextMessageResponse response = new TextMessageResponse();
            if(!Configuration.EnableTextMessage)
            {
                Logger.Info(string.Format("TextMessage is not enable"));
              throw new ArgumentException("TextMessage is not enable");
            }

            Logger.Info(string.Format("SendTextMessage started for {0}", request.PhoneNumber));
            if (string.IsNullOrWhiteSpace(request.CountryCode))
                throw new ArgumentException("Argument is null or whitespace", nameof(request.CountryCode));
            if (string.IsNullOrWhiteSpace(request.PhoneNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(request.PhoneNumber));
            if (string.IsNullOrWhiteSpace(request.Body))
                throw new ArgumentException("Argument is null or whitespace", nameof(request.Body));
            
            TwilioClient.Init(Configuration.AccountSid, Configuration.AuthToken);
            try{
                if(!request.CountryCode.Contains(ConstUtils.CountryCodePlus))
                    request.CountryCode = ConstUtils.CountryCodePlus + request.CountryCode;

                MessageResource message = await MessageResource.CreateAsync(
                    body: request.Body,
                    from: new PhoneNumber(Configuration.FromNumberForText),
                    to: new PhoneNumber(request.CountryCode + request.PhoneNumber)
                );
                response.Sid = message.Sid;

                await EventHubClient.Publish(new Events.TextMessageSend
                {
                    FromPhoneNumber = Configuration.FromNumberForText,
                    ToMobileNumber = request.PhoneNumber,
                    CountryCode = request.CountryCode,
                    Body = request.Body,
                    ResponseData = message
                });

            }
            catch(Exception ex){
                Logger.Error(string.Format("Error occured for {0} in SendTextMessage", request.PhoneNumber), ex);
                throw;
            }
            this.Logger.Info(string.Format("SendTextMessage ended for {0}", request.PhoneNumber));
           return response;
        }
    }
}
