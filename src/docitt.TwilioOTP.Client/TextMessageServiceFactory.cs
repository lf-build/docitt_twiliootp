﻿using System;
using LendFoundry.Security.Tokens;

using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.TwilioOTP.Client
{
    public class TextMessageServiceFactory : ITextMessageServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public TextMessageServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public TextMessageServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public ITextMessageService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("twilio_otp");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new TextMessageService(client);
        }
    }
}