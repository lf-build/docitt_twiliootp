﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using System;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.TwilioOTP.Client
{
    public static class OTPServiceExtensions
    {
       
            [Obsolete("Need to use the overloaded with Uri")]
            public static IServiceCollection AddOTPService(this IServiceCollection services, string endpoint, int port)
            {
                services.AddTransient<IOTPServiceFactory>(p => new OTPServiceFactory(p, endpoint, port));
                services.AddTransient(p => p.GetService<IOTPServiceFactory>().Create(p.GetService<ITokenReader>()));
                return services;
            }

            public static IServiceCollection AddOTPService(this IServiceCollection services, Uri uri)
            {
                services.AddTransient<IOTPServiceFactory>(p => new OTPServiceFactory(p, uri));
                services.AddTransient(p => p.GetService<IOTPServiceFactory>().Create(p.GetService<ITokenReader>()));
                return services;
            }

            public static IServiceCollection AddOTPService(this IServiceCollection services)
            {
                services.AddTransient<IOTPServiceFactory>(p => new OTPServiceFactory(p));
                services.AddTransient(p => p.GetService<IOTPServiceFactory>().Create(p.GetService<ITokenReader>()));
                return services;
            }
        }
    
}
