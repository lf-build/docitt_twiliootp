﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;


namespace Docitt.TwilioOTP.Client
{
    public class OTPService : IOTPService
    {
        private IServiceClient Client { get; }

        public OTPService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        ///  Send OTP
        /// </summary>
        /// <param name="phoneNumber">Phone number to send OTP</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="codeLength">OTP Code Length</param>
        /// <returns>Return OTPResult object</returns>
        public async Task<IOTPResult> SendOTP(string phoneNumber, string countryCode,string codeLength)
        {
            return await Client.GetAsync<OTPResult>("{phoneNumber}/{countryCode}/{code_length}/sendotp");
        }

        /// <summary>
        ///  Verify OTP
        /// </summary>
        /// <param name="phoneNumber">Phone number</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="verificationCode">Verification code</param>
        /// <returns>Return OTPResult object</returns>

        public async Task<IOTPResult> VerifyOTP(string phoneNumber, string countryCode, string verificationCode)
        {
            return await Client.GetAsync<OTPResult>("{phoneNumber}/{countryCode}/{verificationCode}/verify");
        }
    }
}
