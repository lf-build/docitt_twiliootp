﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;


namespace Docitt.TwilioOTP.Client
{
    public class TextMessageService : ITextMessageService
    {
        private IServiceClient Client { get; }

        public TextMessageService(IServiceClient client)
        {
            Client = client;
        }        

         /// <summary>
        /// Send Text Message
        /// </summary>
        /// <param name="request">Text sms request with body</param>
        /// <returns>Text SMS response</returns>
        public async Task<ITextMessageResponse> SendTextMessage(TextMessageRequest request)
        {           
             return await Client.PostAsync<TextMessageRequest,TextMessageResponse>("send", request, true);
        }
    }
}
