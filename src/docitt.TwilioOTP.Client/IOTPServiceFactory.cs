﻿using LendFoundry.Security.Tokens;

namespace Docitt.TwilioOTP.Client
{
    public interface IOTPServiceFactory
    {
        IOTPService Create(ITokenReader reader);
    }
}
