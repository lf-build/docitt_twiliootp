﻿using LendFoundry.Security.Tokens;

namespace Docitt.TwilioOTP.Client
{
    public interface ITextMessageServiceFactory
    {
        ITextMessageService Create(ITokenReader reader);
    }
}
