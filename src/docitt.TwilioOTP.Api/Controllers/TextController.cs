﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;

using System.Threading.Tasks;

namespace Docitt.TwilioOTP.Api.Controllers
{
    /// <summary>
    /// Represents Api controller class.
    /// </summary>
    [Route("/")]
    public class TextController : ExtendedController
    {
        private ITextMessageService TextMessageService { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextController"/> class.
        /// </summary>
        /// <param name="textMessageService"></param>
        /// <param name="logger"></param>
        public TextController(ITextMessageService textMessageService, ILogger logger) : base(logger)
        {
            TextMessageService = textMessageService;
        }
        
        [HttpPost("send")]
        [ProducesResponseType(typeof(ITextMessageResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SendTextMessage([FromBody] TextMessageRequest request)
        {
          
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await TextMessageService.SendTextMessage(request));
                }
                catch(OtpException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
