﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;

using System.Threading.Tasks;

namespace Docitt.TwilioOTP.Api.Controllers
{
    /// <summary>
    /// Represents Api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        private IOTPService OTPService { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="otpService"></param>
        /// <param name="logger"></param>
        public ApiController(IOTPService otpService, ILogger logger) : base(logger)
        {
            OTPService = otpService;
        }

        /// <summary>
        /// SendOTP
        /// </summary>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <param name="countrycode">countrycode</param>
        /// <param name="codelength">codelength</param>
        /// <returns>IOTPResult</returns>
        [HttpGet("{phoneNumber}/{countrycode}/{codelength}/sendotp")]
#if DOTNET2
        [ProducesResponseType(typeof(IOTPResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SendOTP( string phoneNumber, string countrycode,string codelength)
        {
          
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await OTPService.SendOTP(phoneNumber,countrycode,codelength));
                }
                catch(OtpException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// VefifyOTP
        /// </summary>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <param name="countrycode">countrycode</param>
        /// <param name="verificationcode">verificationcode</param>
        /// <returns>IOTPResult</returns>
        [HttpGet("{phoneNumber}/{countrycode}/{verificationcode}/verify/")]
#if DOTNET2
        [ProducesResponseType(typeof(IOTPResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> VefifyOTP(string phoneNumber, string countrycode,string verificationcode)
        {
           
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await OTPService.VerifyOTP(phoneNumber, countrycode, verificationcode));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
